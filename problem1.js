const fs = require("fs");
const path = require("path");

function prob1() {
  fs.mkdir(path.join(__dirname, "jsonDirectory"), (err) => {
    if (err) return console.error(err);
    console.log(`file created successfully`);
  });

  fs.writeFile("./jsonDirectory/abc.json", "world", (err) => {
    if (err) console.error(err);
  });

  let paths = "./jsonDirectory/abc.json";

  fs.unlink(paths, (err) => {
    if (err) console.error(err);
  });
}

module.exports = prob1;
