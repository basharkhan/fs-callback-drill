const fs = require("fs");
const path = require("path");

function prob2() {
  // 1.  Read the given file lipsum.txt

  fs.readFile("./data/lipsum.txt", "utf-8", (err, data) => {
    if (err) console.log(err);

    // 2. Convert the content to uppercase

    data = data.toUpperCase();
    // console.log(data);

    // 2.1 Write content to a new file whose name here is upper.txt

    fs.writeFile("./upper.txt", data, (err) => {
      if (err) console.log(err);
    });

    // 2.2 Store the name of new file in filenames.txt

    fs.appendFile("./filenames.txt", "upper.txt", (err) => {
      if (err) console.log(err);
    });

    // 3. Read the new file

    fs.readFile("./upper.txt", "utf-8", (err, upperData) => {
      if (err) console.log(err);

      lowerData = upperData.toLowerCase();
      //   console.log(lowerData);

      // 3.1 Split contents into sentences.

      splitSentences = lowerData.split(".");

      // stringify the contents

      str = JSON.stringify(splitSentences);

      // 3.2 writing the lowercase data to a new file named lower.txt

      fs.writeFile("./lower.txt", str, (err) => {
        if (err) console.log(err);
      });

      // 3.3 storing the filename i.e.lower.txt in filenames.txt

      fs.appendFile("./filenames.txt", "\nlower.txt", (err) => {
        if (err) console.log(err);
      });

      //   4. Read the new files and sort the content

      fs.readFile("./lower.txt", "utf-8", (err, lowerData1) => {
        lowerData1 = JSON.parse(lowerData1).sort();

        //   console.log(lowerData1);
        lowerData1 = JSON.stringify(lowerData1);

        // 4.1 write to new file , here it's name is sortedLowerData.txt

        fs.writeFile("./sortedLowerData.txt", lowerData1, (err) => {
          if (err) console.log(err);
        });

        fs.appendFile("./filenames.txt", "\nsortedLowerData.txt", (err) => {
          if (err) console.log(err);

          // 5. Read contents of filenames.txt

          fs.readFile("./filenames.txt", "utf-8", (err, fileContent) => {
            let fileArr = fileContent.split("\n");
            fileArr.forEach((element) => {
              fs.unlink(element, (err) => {
                if (err) console.log(err);
              });
            });
            console.log("Files in the filenames.txt deleted successfully");
          });
        });
      });
    });
  });
}

module.exports = prob2;
